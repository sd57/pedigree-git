//!    \file DepthTrackingAction.hh
//!    \brief Declares a class to seed the generator with a random number associated with the track
//!           and to count the number of particles in a generation
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Provided under the Geant4 Software License.

#ifndef DEPTHTRACKINGACTION_HH
#define DEPTHTRACKINGACTION_HH

#include "G4UserTrackingAction.hh"
#include "globals.hh"

class B2RunAction;
class DepthTrackingActionMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DepthTrackingAction : public G4UserTrackingAction {

public:
  DepthTrackingAction(B2RunAction& runAction);
  virtual ~DepthTrackingAction() override {}

  virtual void PreUserTrackingAction(const G4Track*) override;
  void ToggleRestoringSeed(G4bool enable);
private:
  B2RunAction& fRunAction;
  G4bool fRestoreSeed;
  DepthTrackingActionMessenger* fMessenger;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#endif // DEPTHTRACKINGACTION_HH

