//!    \file DepthTrackInformation.hh
//!    \brief Declares a class to store and manage a random seed and generation number \
//!           associated with a track.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Provided under the Geant4 Software License.

#ifndef DEPTHTRACKINFORMATION_HH
#define DEPTHTRACKINFORMATION_HH

#include "globals.hh"
#include "G4Track.hh"
//#include "G4Allocator.hh"
#include "G4VUserTrackInformation.hh"

class DepthTrackInformation : public G4VUserTrackInformation
{
  public:
    DepthTrackInformation();
    DepthTrackInformation(const DepthTrackInformation& aTrackInfo);
    virtual ~DepthTrackInformation();

    void Print() const;
    void IncrementDepth() {++fTrackDepth;}

  private:
    // FIXME multiple responsibilities, TODO split in separate actions
    G4int                 fTrackDepth;
    long fRandomSeed;

  public:
    inline G4int GetTrackDepth() const  {return fTrackDepth;}
    long getRandomSeed() const {return fRandomSeed;}

    void setRandomSeed(long randomSeed) {fRandomSeed = randomSeed;}

};

#endif // DEPTHTRACKINFORMATION_HH

