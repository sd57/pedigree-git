//!    \file AnalysisManager.hh.hh
//!    \brief Chooses the analysis manager for the example.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Provided under the Geant4 Software License.

#ifndef ANALYSISMANAGER_HH
#define ANALYSISMANAGER_HH

#include "g4root.hh"

#endif // ANALYSISMANAGER_HH
