//!    \file RandomSeedSteppingAction.hh
//!    \brief Declares a class to set a random seed to begin the secondary tracking with.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Provided under the Geant4 Software License.

#ifndef RandomSeedSteppingAction_HH
#define RandomSeedSteppingAction_HH

#include "G4UserSteppingAction.hh"

class B2RunAction;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class RandomSeedSteppingAction : public G4UserSteppingAction {

public:
  RandomSeedSteppingAction(){}
  virtual ~RandomSeedSteppingAction() override {}

  virtual void UserSteppingAction(const G4Step* aStep) override;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#endif // RandomSeedSteppingAction_HH

