//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B2RunAction.hh 66536 2012-12-19 14:32:36Z ihrivnac $
//
/// \file B2RunAction.hh
/// \brief Definition of the B2RunAction class

#ifndef B2RunAction_h
#define B2RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

#include "AnalysisManager.hh"
#include <vector>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4Run;
class B2bRunActionMessenger;

/// Run action class

class B2RunAction : public G4UserRunAction
{
  public:
    B2RunAction(G4AnalysisManager* analysisManager, G4int aNumberOfBins = 200);
    virtual ~B2RunAction() override;

    virtual void BeginOfRunAction(const G4Run* run) override;
    virtual void   EndOfRunAction(const G4Run* aRun) override;
    void addParticleToGeneration(G4int generation);
    void SetGenHistLimit(G4int numberOfBins);
private:
    static G4int fTotalMaxGeneration;
    G4int fMaxGeneration;
    G4int fNumberOfBins;
    G4AnalysisManager* fAnalysisManager;
    B2bRunActionMessenger* fMessenger;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
