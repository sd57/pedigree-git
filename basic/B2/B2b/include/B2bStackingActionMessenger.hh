//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file B2bStackingActionMessenger.hh
/// \brief Definition of the B2bStackingActionMessenger class

#ifndef B2bStackingActionMessenger_h
#define B2bStackingActionMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class B3StackingAction;
class G4UIdirectory;
class G4UIcmdWithAString;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/// Messenger class that defines commands for B2bStackingAction.
///
/// It implements commands:
/// - /B2/Stacking/setHistogramLimit int

class B2bStackingActionMessenger: public G4UImessenger
{
  public:
    B2bStackingActionMessenger(B3StackingAction* Stacking);
    virtual ~B2bStackingActionMessenger();
    
    virtual void SetNewValue(G4UIcommand*, G4String);
    
  private:
    B3StackingAction*  fStackingAction;

    G4UIdirectory*           fB2Directory;
    G4UIdirectory*           fStackingDirectory;

    G4UIcmdWithAString* fStackingTypeCmd;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
