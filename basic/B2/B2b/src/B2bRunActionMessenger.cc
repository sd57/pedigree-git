//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
// 
/// \file B2bRunActionMessenger.cc
/// \brief Implementation of the B2bRunActionMessenger class

#include "B2bRunActionMessenger.hh"
#include "B2RunAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2bRunActionMessenger::B2bRunActionMessenger(B2RunAction* Run)
 : G4UImessenger(),
   fRunAction(Run)
{
  fB2Directory = new G4UIdirectory("/B2/");
  fB2Directory->SetGuidance("UI commands specific to this example.");

  fRunDirectory = new G4UIdirectory("/B2/run/");
  fRunDirectory->SetGuidance("RunAction control");

  fGenMaxCmd = new G4UIcmdWithAnInteger("/B2/run/setGenHistLimit",this);
  fGenMaxCmd->SetGuidance("Select the number of bins \
                          for the generation population histogram");
  fGenMaxCmd->SetParameterName("genMax",false);
  fGenMaxCmd->SetDefaultValue(2);
  fGenMaxCmd->SetRange("genMax >0");
  fGenMaxCmd->SetToBeBroadcasted(true);
  fGenMaxCmd->AvailableForStates(G4State_PreInit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2bRunActionMessenger::~B2bRunActionMessenger()
{
  delete fGenMaxCmd;
  delete fB2Directory;
  delete fRunDirectory;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2bRunActionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == fGenMaxCmd ) {
    fRunAction->SetGenHistLimit(fGenMaxCmd->GetNewIntValue(newValue));
  }   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
