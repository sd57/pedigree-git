//!    \file DepthTrackInformation.cc
//!    \brief Implements a class to store and manage a random seed and generation number \
//!           associated with a track.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Provided under the Geant4 Software License.

#include "DepthTrackInformation.hh"
#include "G4ios.hh"

DepthTrackInformation::DepthTrackInformation(): fTrackDepth(0)
{}

DepthTrackInformation::DepthTrackInformation(const DepthTrackInformation& aTrackInfo)
  : G4VUserTrackInformation(), fTrackDepth(aTrackInfo.GetTrackDepth()),
    fRandomSeed(aTrackInfo.getRandomSeed())
{}

DepthTrackInformation::~DepthTrackInformation(){;}

void DepthTrackInformation::Print() const
{
    G4cout << "The track depth is " << fTrackDepth;
    G4cout << ", the random seed is " << fRandomSeed << G4endl;
}


