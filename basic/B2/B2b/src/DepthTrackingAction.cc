//!    \file DepthTrackingAction.cc
//!    \brief Implements a class to seed the generator with a random number associated with the track
//!           and to count the number of particles in a generation
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Provided under the Geant4 Software License.

#include "DepthTrackingAction.hh"
#include "DepthTrackingActionMessenger.hh"
#include "G4TrackingManager.hh"
#include "G4Track.hh"
#include "DepthTrackInformation.hh"
#include "B2RunAction.hh"
#include "G4MTHepRandom.hh"
#include "CLHEP/Random/RandomEngine.h"

//#define DEBUGDEPTHTRACKINGACTION

DepthTrackingAction::DepthTrackingAction(B2RunAction& runAction):
  fRunAction(runAction), fRestoreSeed(false), fMessenger(new DepthTrackingActionMessenger(this))
{}

void DepthTrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
  CLHEP::HepRandomEngine * randomEngine = G4MTHepRandom::getTheEngine();

  if(aTrack->GetParentID() == 0 && aTrack->GetUserInformation() == nullptr)
  {
    DepthTrackInformation* anInfo = new DepthTrackInformation();
    if(fRestoreSeed)
    {
      long randomSeed = randomEngine->getSeed();
      anInfo->setRandomSeed(randomSeed);
    }
    aTrack->SetUserInformation(anInfo);
  }

  DepthTrackInformation const* userInfo = static_cast<DepthTrackInformation*>(
                                      aTrack->GetUserInformation());
  if(!userInfo)
  {
    G4cout << "DepthTrackingAction::PreUserTrackingAction: Warning: "
           << "no user information associated with the trackID = "
           << aTrack->GetTrackID() << G4endl;
  }
  else
  {
    G4int const generation = userInfo->GetTrackDepth();
    fRunAction.addParticleToGeneration(generation);
#ifdef DEBUGDEPTHTRACKINGACTION
    G4cout << "DepthTrackingAction::PreUserTrackingAction: trackID = "
           << aTrack->GetTrackID() << "; ";
    userInfo->Print();
#endif
    if(fRestoreSeed)
    {
      if(aTrack->GetParentID() != 0)
      {
        auto const randomSeed = userInfo->getRandomSeed();
        randomEngine->setSeed(randomSeed, 0);
      }
    }
  }
}

void DepthTrackingAction::ToggleRestoringSeed(G4bool enable)
{
  fRestoreSeed = enable;
}

