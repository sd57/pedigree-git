//!    \file RandomSeedSteppingAction.cc
//!    \brief Implements a class to set a random seed to begin the secondary tracking with.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Provided under the Geant4 Software License.

#include "RandomSeedSteppingAction.hh"
#include "G4SteppingManager.hh"
#include "G4Track.hh"
#include "DepthTrackInformation.hh"
#include "G4MTHepRandom.hh"
#include "CLHEP/Random/RandomEngine.h"

//#define DEBUGRandomSeedSteppingAction

void RandomSeedSteppingAction::UserSteppingAction(const G4Step* aStep)
{
  G4Track* aTrack = aStep->GetTrack();
  const std::vector<const G4Track*>* secondaries = aStep->GetSecondaryInCurrentStep();
  if(secondaries)
  {
    DepthTrackInformation& info = *static_cast<DepthTrackInformation*>(
                                    aTrack->GetUserInformation());
    for(auto& secondary : *secondaries)
    {
      DepthTrackInformation* infoNew = new DepthTrackInformation(info);
      infoNew->IncrementDepth();

      CLHEP::HepRandomEngine * randomEngine = G4MTHepRandom::getTheEngine();

      long randomSeed = (unsigned int)(*randomEngine);
      infoNew->setRandomSeed(randomSeed);
      secondary->SetUserInformation(infoNew);
    }
  }
}
