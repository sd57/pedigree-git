//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
// 
/// \file B2bStackingActionMessenger.cc
/// \brief Implementation of the B2bStackingActionMessenger class

#include "B2bStackingActionMessenger.hh"
#include "B3StackingAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

#include <map>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

namespace
{
// TODO make compile-time
  std::map<G4String, B3StackingAction::StackingType> const theStackingTypesMap
  {
    {"default", B3StackingAction::StackingType::fDefault},
    {"random", B3StackingAction::StackingType::fRandom},
    {"queue", B3StackingAction::StackingType::fQueue},
  };
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2bStackingActionMessenger::B2bStackingActionMessenger(B3StackingAction* Stacking)
 : G4UImessenger(),
   fStackingAction(Stacking)
{
  fB2Directory = new G4UIdirectory("/B2/");
  fB2Directory->SetGuidance("UI commands specific to this example.");

  fStackingDirectory = new G4UIdirectory("/B2/stack/");
  fStackingDirectory->SetGuidance("StackingAction control");

  fStackingTypeCmd = new G4UIcmdWithAString("/B2/stack/setStackingType",this);
  fStackingTypeCmd->SetGuidance("Select the stacking order");
  fStackingTypeCmd->SetParameterName("stackingType",false);
  fStackingTypeCmd->SetDefaultValue("default");
  G4String const candidateString = []()
  {
    G4String str;
    for(auto const& p : theStackingTypesMap)
    {
      str += p.first;
      str += " ";
    }
    return str;
  }();

  fStackingTypeCmd->SetCandidates(candidateString);
  fStackingTypeCmd->SetToBeBroadcasted(true);
  fStackingTypeCmd->AvailableForStates(G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2bStackingActionMessenger::~B2bStackingActionMessenger()
{
  delete fStackingTypeCmd;
  delete fB2Directory;
  delete fStackingDirectory;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2bStackingActionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == fStackingTypeCmd ) {
    fStackingAction->SetStackingType(theStackingTypesMap.at(newValue));
  }   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
