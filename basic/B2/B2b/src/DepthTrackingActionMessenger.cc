//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
// 
/// \file DepthTrackingActionMessenger.cc
/// \brief Implementation of the DepthTrackingActionMessenger class

#include "DepthTrackingActionMessenger.hh"
#include "DepthTrackingAction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"

#include <map>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DepthTrackingActionMessenger::DepthTrackingActionMessenger(DepthTrackingAction* DepthTracking)
 : G4UImessenger(),
   fDepthTrackingAction(DepthTracking)
{
  fB2Directory = new G4UIdirectory("/B2/");
  fB2Directory->SetGuidance("UI commands specific to this example.");

  fDepthTrackingDirectory = new G4UIdirectory("/B2/track/");
  fDepthTrackingDirectory->SetGuidance("DepthTrackingAction control");

  fRestoreTrackSeedsCmd = new G4UIcmdWithABool("/B2/track/restoreTrackSeeds",this);
  fRestoreTrackSeedsCmd->SetGuidance("Select if the track seeds are restored");
  fRestoreTrackSeedsCmd->SetParameterName("setSaveTrackSeeds",false);
  fRestoreTrackSeedsCmd->SetDefaultValue(false);
  fRestoreTrackSeedsCmd->SetToBeBroadcasted(true);
  fRestoreTrackSeedsCmd->AvailableForStates(G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DepthTrackingActionMessenger::~DepthTrackingActionMessenger()
{
  delete fRestoreTrackSeedsCmd;
  delete fB2Directory;
  delete fDepthTrackingDirectory;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DepthTrackingActionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == fRestoreTrackSeedsCmd ) {
    fDepthTrackingAction->ToggleRestoringSeed(fRestoreTrackSeedsCmd->GetNewBoolValue(newValue));
  }   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
