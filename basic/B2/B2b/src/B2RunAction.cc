//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B2RunAction.cc 66536 2012-12-19 14:32:36Z ihrivnac $
//
/// \file B2RunAction.cc
/// \brief Implementation of the B2RunAction class

#include "B2RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "B2bRunActionMessenger.hh"
#include "G4AutoLock.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int B2RunAction::fTotalMaxGeneration = 0;

B2RunAction::B2RunAction(G4AnalysisManager* analysisManager,
                         G4int aNumberOfBins):
     G4UserRunAction(), fMaxGeneration(-1), fNumberOfBins(aNumberOfBins),
  fAnalysisManager(analysisManager), fMessenger(new B2bRunActionMessenger(this))
{
  // set printing event number per each 100 events
  G4RunManager::GetRunManager()->SetPrintProgress(1000);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2RunAction::~B2RunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2RunAction::BeginOfRunAction(const G4Run* run)
{
  G4int const runID = run->GetRunID();
  if(!runID)
  {
    G4cout << "Creating histogram with " << fNumberOfBins << " bins" << G4endl;
    fAnalysisManager->CreateH1("genPop", "generation population",
                               fNumberOfBins, -0.5, fNumberOfBins - 0.5);
  }
  //inform the runManager to save random number seed
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);
  G4String const fileName = std::string("genPop") + std::to_string(runID);
  fAnalysisManager->ScaleH1(0, 0);
  fAnalysisManager->OpenFile(fileName);
  fMaxGeneration = -1;
  if(!isMaster) fTotalMaxGeneration = -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2RunAction::EndOfRunAction(const G4Run*)
{
  fAnalysisManager->Write();
  fAnalysisManager->CloseFile();
  auto checkMaxGeneration = [this]()
  {
    static G4Mutex mutex;
    G4AutoLock lock(&mutex);
    if( fMaxGeneration > fTotalMaxGeneration)
      fTotalMaxGeneration = fMaxGeneration;
  };
  checkMaxGeneration();
  if(!isMaster)
  {
    if(fMaxGeneration >= 0)
    {
      G4cout << "B2RunAction::EndOfRunAction: "
             << "the maximum generation in this thread is "
             << fMaxGeneration << G4endl;
    }
    else
    {
      G4cout << "B2RunAction::EndOfRunAction: "
             << "no particles were generated in this thread"
             << G4endl;
    }
  }
  else
  {
    G4cout << "B2RunAction::EndOfRunAction: "
           << "the maximum generation in all threads is "
           << fTotalMaxGeneration << G4endl;
  }

}

void B2RunAction::addParticleToGeneration(G4int generation)
{
  if(generation < 0)
  {
    G4cout << "B2RunAction::addParticleToGeneration: Warning: "
           << "neagtive generation = " << generation << G4endl;
    return;
  }
  else
  {
    if(generation > fMaxGeneration)
    {
      fMaxGeneration = generation;
    }
  }
  if(generation >= fNumberOfBins)
  {
#ifdef B2RUNACTIONDEBUG
    G4cout << "B2RunAction::addParticleToGeneration: Warning: "
               << "the generation number (" << generation
               <<") exceeds the histogram limit (" << fNumberOfBins << ")"
              << G4endl;
#endif
    generation = fNumberOfBins - 1;
  }
  fAnalysisManager->FillH1(0, generation);
}

void B2RunAction::SetGenHistLimit(G4int numberOfBins)
{
  if(numberOfBins <= 0)
  {
#ifdef B2RUNACTIONDEBUG
    G4cout << "B2RunAction::SetGenHistLimit: Warning: "
           << "bad upper histogram limit " << maxGen
           << ", must be greater than zero" << G4endl;
#endif
    return;
  }
  fNumberOfBins = numberOfBins;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
