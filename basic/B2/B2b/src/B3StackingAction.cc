//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B3StackingAction.cc 66536 2012-12-19 14:32:36Z ihrivnac $
// 
/// \file B3StackingAction.cc
/// \brief Implementation of the B3StackingAction class

#include "B3StackingAction.hh"
#include "B2bStackingActionMessenger.hh"

#include <random>


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

namespace
{
template<B3StackingAction::StackingType aStackingType = B3StackingAction::StackingType::fDefault>
G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
template<>
G4ClassificationOfNewTrack ClassifyNewTrack<B3StackingAction::StackingType::fDefault>(const G4Track*);
template<>
G4ClassificationOfNewTrack ClassifyNewTrack<B3StackingAction::StackingType::fRandom>(const G4Track*);

}  // anonymous namespace

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B3StackingAction::B3StackingAction(): fStackingType(StackingType::fDefault),
  fMessenger(new B2bStackingActionMessenger(this))
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B3StackingAction::~B3StackingAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4ClassificationOfNewTrack
B3StackingAction::ClassifyNewTrack(const G4Track* aTrack)
{
  if(fStackingType == StackingType::fDefault)
    return ::ClassifyNewTrack<StackingType::fDefault>(aTrack);
  else if(fStackingType == StackingType::fRandom)
    return ::ClassifyNewTrack<StackingType::fRandom>(aTrack);
  else
  {
#warning "Unimplemented"
    return fUrgent;
  }
}

void B3StackingAction::SetStackingType(StackingType aStackingType)
{
  fStackingType = aStackingType;
}

namespace
{
template<>
G4ClassificationOfNewTrack ClassifyNewTrack<B3StackingAction::StackingType::fDefault>(const G4Track*)
{
  return fUrgent;
}

template<>
G4ClassificationOfNewTrack ClassifyNewTrack<B3StackingAction::StackingType::fRandom>(const G4Track*)
{
#warning "Untested"
  G4ThreadLocalStatic std::random_device aRandomDevice;
  G4ThreadLocalStatic std::uniform_int_distribution<int> aDistribution(0, 1);
  // FIXME implicitly uses the values of fUrgent and fWaiting
  auto const theChosenStack = aDistribution(aRandomDevice);
  return G4ClassificationOfNewTrack(theChosenStack);
}

} // anonymous namespace


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
