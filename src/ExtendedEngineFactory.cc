//!    \file ExtendedEngineFactory.cc
//!    \brief Implements an engine factory with support for additional engines.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "ExtendedEngineFactory.hh"
#include "G4PCbprng.hh"
#include "CLHEP/Random/engineIDulong.h"

#include "random123.hpp"

namespace CLHEP
{

// FIXME copy-paste from EngineFactory.cc
template<class E>
static HepRandomEngine*
makeAnEngine (const std::string & tag,
              std::istream & is) {
  if ( tag != E::beginTag() ) return 0;
  HepRandomEngine* eptr = new E;
  eptr->getState(is);
  if (!is) return 0;
  return eptr;
}

template<class E>
static HepRandomEngine*
makeAnEngine (const std::vector<unsigned long> & v) {
  if ( (v[0] & 0xffffffffUL) != engineIDulong<E>() ) return 0;
  HepRandomEngine* eptr = new E;
  bool success = eptr->getState(v);
  if (!success) return 0;
  // std::cerr << "makeAnEngine made " << E::engineName() << "\n";
  return eptr;
}

HepRandomEngine* ExtendedEngineFactory::newEngine(std::istream& is) {
  HepRandomEngine* eptr;
  std::string tag;
  is >> tag;
  eptr = makeAnEngine <G4PCbprng<r123::Philox2x32>>  (tag, is); if (eptr) return eptr;
  eptr = makeAnEngine <G4PCbprng<r123::Threefry2x32>>  (tag, is); if (eptr) return eptr;
  return EngineFactory::newEngine(is);
}

HepRandomEngine*
ExtendedEngineFactory::newEngine(std::vector<unsigned long> const & v) {
  HepRandomEngine* eptr;
  eptr = makeAnEngine <G4PCbprng<r123::Philox2x32>>  (v); if (eptr) return eptr;
  eptr = makeAnEngine <G4PCbprng<r123::Threefry2x32>>  (v); if (eptr) return eptr;
  return EngineFactory::newEngine(v);
}


}
