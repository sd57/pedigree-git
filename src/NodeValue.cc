//!    \file NodeValue.cc
//!    \brief Defines the class for the generator state with additional information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "NodeValue.hh"
#include "RNGState.hh"
#include "CLHEP/Random/RandomEngine.h"

#include <cmath>

#include <iostream>

namespace
{
constexpr bool debugOutput = false;
size_t HashFucnction(size_t E, size_t H);
}

NodeValue::NodeValue(int e, size_t aHashValue, const StateType &rngState):
  fE(e), fHashedPedigree(aHashValue), fRNGState(new StateType(rngState)),
  fNumberOfChildren(0)
{}

NodeValue::NodeValue(int e, size_t aHashValue, EngineType* engine):
  fE(e), fHashedPedigree(aHashValue),
  fNumberOfChildren(0)
{
  engine->setSeed(static_cast<long>(aHashValue), -1);
  fRNGState = new StateType(engine->put());
}

NodeValue::~NodeValue()
{
  delete fRNGState;
  if(debugOutput)
    std::cout << "NodeValue::~NodeValue(): deleting node at "
              << static_cast<void*>(this) << std::endl;
}

NodeValue::NodeValue(const NodeValue &value):
  fE(value.fE), fHashedPedigree(value.fHashedPedigree),
  fRNGState(new StateType(*value.fRNGState)),
  fNumberOfChildren(value.fNumberOfChildren)
{}

NodeValue NodeValue::SpawnNode(uint maxN, EngineType* engine)
{
  auto const engineState = engine->put();
  double const randomValue = [this, engine, engineState](){
    engine->get(fRNGState->GetEngineState());
    auto value = double(*engine);
    *fRNGState = engine->put();
    return value;
  }();
  double const dampingFactor = 0.5/maxN;
  int const newE = static_cast<int>(std::floor( dampingFactor * randomValue * fE ));
  size_t const newHashValue = HashFucnction(fHashedPedigree, fNumberOfChildren++);
  return NodeValue(newE, newHashValue, engine);
}

const NodeValue::StateType* NodeValue::GetRNGState() const
{
  return fRNGState;
}

bool operator==(NodeValue const& rhs, NodeValue const& lhs)
{
  return lhs.GetE() == rhs.GetE() &&
      lhs.GetHashValue() == rhs.GetHashValue() &&
      lhs.GetNumberOfChildren() == rhs.GetNumberOfChildren() &&
      *lhs.GetRNGState() == *rhs.GetRNGState();
}

namespace
{
  size_t HashFucnction(size_t E, size_t H)
  {
    static const std::hash<size_t> standardHashSizetFunction;
    // TODO use the hash from boost
    return standardHashSizetFunction(E) ^ standardHashSizetFunction(H);
  }
}


