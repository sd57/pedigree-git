//!    \file GenealogicTree.cc
//!    \brief Defines a tree structure that hold nodes containing pedigree information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "GenealogicTree.hh"
#include "NodeValue.hh"
#include "RNGState.hh"

#include "ExtendedEngineFactory.hh"
#include <algorithm>

GenealogicTree::GenealogicTree(RandomEngine const& engine, uint maxN):
  fMaxN(maxN)
{
  fRandomEngine = CLHEP::ExtendedEngineFactory::newEngine(engine.put());
  if(!fRandomEngine)
  {
    std::cout << "GenealogicTree::GenealogicTree: failed to construct fRandomEngine, will fail" << std::endl;
  }
  else fRandomEngine->get(engine.put());
}

GenealogicTree::~GenealogicTree()
{
  for(auto it: data) delete it;
  delete fRandomEngine;
}

GenealogicTree::NodeType *GenealogicTree::SpawnNode(NodeType *node)
{
  auto const newNode = node->SpawnNode(fMaxN, fRandomEngine);
  data.push_back(new NodeValue(std::move(newNode)));
  return data.back();
}

GenealogicTree::NodeType *GenealogicTree::EmplaceTopNode(int e, size_t hashValue)
{
  fRandomEngine->setSeed(static_cast<long>(hashValue), -1);
  data.push_back(new NodeValue(e, hashValue, fRandomEngine->put()));
  return data.back();
}

GenealogicTree::RandomEngine const* GenealogicTree::GetRandomEngine() const
{
  return fRandomEngine;
}

size_t GenealogicTree::GetTotalNumberOfNodes() const
{
  return data.size();
}

bool operator==(GenealogicTree const& rhs, GenealogicTree const& lhs)
{
  // expanded for debug purposes
  auto const maxNAreEqual = [&rhs, &lhs]()
  {
    return lhs.GetMaxN() == rhs.GetMaxN();
  };
  auto const numbersOfNodesAreEqual = [&rhs, &lhs]()
  {
    return lhs.GetTotalNumberOfNodes() == rhs.GetTotalNumberOfNodes();
  };
  auto const randomEnginesAreEqual = [&rhs, &lhs]()
  {
    // TODO compare the types
    auto const * const lRNG = lhs.GetRandomEngine();
    auto const * const rRNG = rhs.GetRandomEngine();
    auto const ldata = lRNG->put();
    auto const rdata = rRNG->put();
    return ldata == rdata;
  };
  auto const dataIsEqual = [&rhs, &lhs]()
  {
    auto& rData = rhs.GetInternalData();
    auto& lData = lhs.GetInternalData();
    return std::is_permutation(lData.begin(), lData.end(),
                               rData.begin(), rData.end(),
                               [](NodeValue* l, NodeValue* r){return *l == *r;});
  };
  return maxNAreEqual() &&
      numbersOfNodesAreEqual() &&
      randomEnginesAreEqual() && // heavy, evaluate only if the others succeed
      dataIsEqual(); // heavy, evaluate only if the others succeed
}
