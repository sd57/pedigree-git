//!    \file CGenealogicTree.cc
//!    \brief Defines a tree structure that hold nodes containing pedigree information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "CGenealogicTree.hh"
#include "CNodeValue.hh"
#include "CRNGState.hh"

#include "random123.hpp"

template<typename CBPRNG_t>
CGenealogicTree<CBPRNG_t>::CGenealogicTree(uint maxN): fMaxN(maxN),
  fRandomEngine(new CBPRNG_t)
{}

template<typename CBPRNG_t>
CGenealogicTree<CBPRNG_t>::~CGenealogicTree()
{
  delete fRandomEngine;
  for(auto it: data) delete it;
}

template<typename CBPRNG_t>
typename CGenealogicTree<CBPRNG_t>::NodeType* CGenealogicTree<CBPRNG_t>::SpawnNode(NodeType *node)
{
  auto const newNode = node->SpawnNode(fMaxN, fRandomEngine);
  data.push_back(new NodeType(std::move(newNode)));
  return data.back();
}

template<typename CBPRNG_t>
typename CGenealogicTree<CBPRNG_t>::NodeType*
CGenealogicTree<CBPRNG_t>::EmplaceTopNode(int e, uint64_t aHashValue)
{
  data.push_back(new NodeType(e, aHashValue));
  return data.back();
}

template<typename CBPRNG_t>
const typename CGenealogicTree<CBPRNG_t>::RandomEngine *CGenealogicTree<CBPRNG_t>::GetRandomEngine() const
{
  return fRandomEngine;
}

template<typename CBPRNG_t>
size_t CGenealogicTree<CBPRNG_t>::GetTotalNumberOfNodes() const
{
  return data.size();
}

template<typename CBPRNG_t>
bool operator==(CGenealogicTree<CBPRNG_t> const& rhs, CGenealogicTree<CBPRNG_t> const& lhs)
{
  // expanded for debug purposes
  auto const maxNAreEqual = [&rhs, &lhs]()
  {
    return lhs.GetMaxN() == rhs.GetMaxN();
  };
  auto const numbersOfNodesAreEqual = [&rhs, &lhs]()
  {
    return lhs.GetTotalNumberOfNodes() == rhs.GetTotalNumberOfNodes();
  };
  auto const dataIsEqual = [&rhs, &lhs]()
  {
    auto const& rData = rhs.GetInternalData();
    auto const& lData = lhs.GetInternalData();
    auto const result = std::is_permutation(lData.begin(), lData.end(),
                                            rData.begin(), rData.end(),
                                            [](typename CGenealogicTree<CBPRNG_t>::NodeType* l,
                                                        typename CGenealogicTree<CBPRNG_t>::NodeType* r){return *l == *r;});
    return result;
  };
  return maxNAreEqual() &&
      numbersOfNodesAreEqual() &&
      dataIsEqual(); // heavy, evaluate only if the others succeed
}

template class CGenealogicTree<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>>;
template class CGenealogicTree<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>>;

template bool operator==(CGenealogicTree<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>> const& rhs,
                         CGenealogicTree<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>> const& lhs);

template bool operator==(CGenealogicTree<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>> const& rhs,
                         CGenealogicTree<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>> const& lhs);
