//!    \file CRNGState.cc
//!    \brief Defines the class storing both the key and the counter for a Random123 engine.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "CRNGState.hh"
#include "random123.hpp"
#include <iostream>

namespace
{
constexpr bool debugOutput = false;
}

template<typename CBPRNG_t>
CRNGState<CBPRNG_t>::CRNGState(ctr_type aCtr, key_type aKey): fCtr{aCtr}, fKey{aKey}
{
  if(debugOutput)
    std::cout << "CRNGState::CRNGState(int): initializing CRNGState at "
              << static_cast<void*>(this)
              << std::endl;
}


template<typename CBPRNG_t>
CRNGState<CBPRNG_t>::CRNGState(CRNGState &&state) :
  fCtr(std::move(state.fCtr)), fKey(std::move(state.fKey))
{
  if(debugOutput)
    std::cout << "CRNGState::CRNGState(CRNGState&&): moving CRNGState from "
              << static_cast<void*>(&state)
              << " to " << static_cast<void*>(this)
              << std::endl;
}

template<typename CBPRNG_t>
CRNGState<CBPRNG_t>::CRNGState(CRNGState const& state) :
  fCtr(state.fCtr), fKey(state.fKey)
{
  if(debugOutput)
    std::cout << "CRNGState::CRNGState(CRNGState const&): copying CRNGState from "
              << static_cast<void const*>(&state)
              << ") to " << static_cast<void*>(this)
              << std::endl;
}

template<typename CBPRNG_t>
CRNGState<CBPRNG_t>::~CRNGState()
{
  if(debugOutput)
    std::cout << "CRNGState::~CRNGState(): deleting CRNGState at "
              << static_cast<void*>(this)
              << std::endl;
}

template<typename CBPRNG_t>
void CRNGState<CBPRNG_t>::IncreaseKey()
{
  // increment the parts of the key starting from the back
  auto it = fKey.rbegin();
  ++(*it);
  while(!*(it++) && it != fKey.rend()) ++(*it);
}

template<typename CBPRNG_t>
void CRNGState<CBPRNG_t>::IncreaseCtr()
{
  // increment the parts of the counter starting from the back
  auto it = fCtr.rbegin();
  ++(*it);
  while(!*(it++) && it != fCtr.rend()) ++(*it);
}

template<typename CBPRNG_t>
typename CRNGState<CBPRNG_t>::ctr_type CRNGState<CBPRNG_t>::GetCtr()
{
  return fCtr;
}

template<typename CBPRNG_t>
typename CRNGState<CBPRNG_t>::key_type CRNGState<CBPRNG_t>::GetKey()
{
  return fKey;
}

template<typename CBPRNG_t>
void CRNGState<CBPRNG_t>::SetCtr(ctr_type const& aCtr)
{
  fCtr = aCtr;
}

template<typename CBPRNG_t>
bool CRNGState<CBPRNG_t>::operator==(CRNGState const& rhs) const
{
  return fCtr == rhs.fCtr && fKey == rhs.fKey;
}


template class CRNGState<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>>;
template class CRNGState<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>>;
