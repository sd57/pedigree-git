//!    \file RNGState.cc
//!    \brief Defines the class storing both the key and the counter for a Random123 engine.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "RNGState.hh"
#include <iostream>

namespace
{
constexpr bool debugOutput = false;
}

RNGState::RNGState()
{
  if(debugOutput)
    std::cout << "RNGState::RNGState(int): initializing RNGState at "
              << static_cast<void*>(this)
              << std::endl;
}

RNGState::RNGState(RNGState&& state) :
  fEngineState(std::move(state.fEngineState))
{
  if(debugOutput)
    std::cout << "RNGState::RNGState(RNGState&&): moving RNGState from "
              << static_cast<void*>(&state)
              << " to " << static_cast<void*>(this)
              << std::endl;
}

RNGState::RNGState(RNGState const& state) :
  fEngineState(state.fEngineState)
{
  if(debugOutput)
    std::cout << "RNGState::RNGState(RNGState const&): copying RNGState from "
              << static_cast<void const*>(&state)
              << ") to " << static_cast<void*>(this)
              << std::endl;
}

RNGState const& RNGState::operator=(RNGState const& state)
{
  fEngineState = state.fEngineState;
  return *this;
}

RNGState::~RNGState()
{
  if(debugOutput)
    std::cout << "RNGState::~RNGState(): deleting RNGState at "
              << static_cast<void*>(this)
              << std::endl;
}

bool RNGState::operator==(RNGState const& rhs) const
{
  return fEngineState == rhs.fEngineState;
}

RNGState::RNGState(const RandomEngine &engine)
  : RNGState()
{
  fEngineState = engine.put();
}

RNGState::RNGState(CLHEPStateType const& engineState)
  : fEngineState(engineState)
{}
