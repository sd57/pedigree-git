//!    \file CNodeValue.cc
//!    \brief Defines the class for the generator state with additional information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "CNodeValue.hh"
#include "CRNGState.hh"
#include "CLHEP/Random/RandomEngine.h"
#include "random123.hpp"
#include <cmath>
#include <iostream>

namespace
{
  size_t HashFucnction(size_t E, size_t H);
  constexpr bool debugOutput = false;
}

template<typename CBPRNG_t>
CNodeValue<CBPRNG_t>::CNodeValue(int e, size_t aHashValue,
                                 const StateType *crngState):
  fE(e), fHashedPedigree(aHashValue),
  fCRNGState(crngState ? new StateType(*crngState)
                       : new StateType(typename StateType::ctr_type({{aHashValue}}))),
  fNumberOfChildren(0)
{}

template<typename CBPRNG_t>
CNodeValue<CBPRNG_t>::~CNodeValue()
{
  delete fCRNGState;
  if(debugOutput)
    std::cout << "CNodeValue::~CNodeValue(): deleting node at "
              << static_cast<void*>(this) << std::endl;
}

template<typename CBPRNG_t>
CNodeValue<CBPRNG_t>::CNodeValue(const CNodeValue &value):
  fE(value.fE), fHashedPedigree(value.fHashedPedigree),
  fCRNGState(new StateType(*value.fCRNGState)),
  fNumberOfChildren(value.fNumberOfChildren)
{}

template<typename CBPRNG_t>
CNodeValue<CBPRNG_t> CNodeValue<CBPRNG_t>::SpawnNode(uint32_t maxN, CBPRNG_t* aRandomEngine)
{
  double const randomValue = [this, aRandomEngine](){
    auto const randVal = (*aRandomEngine)(fCRNGState->GetCtr(), fCRNGState->GetKey()).at(0);
    double const value = r123::u01<double>(randVal);
    fCRNGState->IncreaseKey();
    return value;
  }();
  double const dampingFactor = 0.5/maxN;
  // CHECKME decreases E_i each time, E_i += i/(maxN+1) seems more logical
  // can reach DBL_MIN very quickly and yield 0 for each node
  int const newE = static_cast<int>(std::floor( dampingFactor * randomValue * fE ));
  auto const newHashValue = HashFucnction(fHashedPedigree, fNumberOfChildren++);
  return CNodeValue(newE, newHashValue);
}

template<typename CBPRNG_t>
typename CNodeValue<CBPRNG_t>::StateType const* CNodeValue<CBPRNG_t>::GetRNGState() const
{
  return fCRNGState;
}

template<typename CBPRNG_t>
bool operator==(CNodeValue<CBPRNG_t> const& rhs, CNodeValue<CBPRNG_t> const& lhs)
{
  return lhs.GetE() == rhs.GetE() &&
      lhs.GetNumberOfChildren() == rhs.GetNumberOfChildren() &&
      lhs.GetHashValue() == rhs.GetHashValue() &&
      *lhs.GetRNGState() == *rhs.GetRNGState();
}

template class CNodeValue<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>>;
template class CNodeValue<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>>;

template bool operator==(CNodeValue<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>> const& rhs,
                         CNodeValue<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>> const& lhs);

template bool operator==(CNodeValue<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>> const& rhs,
                         CNodeValue<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>> const& lhs);

namespace
{
  size_t HashFucnction(size_t E, size_t H)
  {
    static const std::hash<size_t> standardHashSizetFunction;
    // TODO use the hash from boost
    return standardHashSizetFunction(E) ^ standardHashSizetFunction(H);
  }
}
