//!    \file CGenealogicTree.hh
//!    \brief Tree structure that hold nodes containing pedigree information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.
//!
//!    The structure is a tree from tree.hh library by Kasper Peeters.
//!    It allows initializing the root node with a parameter to produce different trees
//!    and spawning daughter nodes.
//!    The state of the subsequent nodes is defined by the number of previously spawn siblings
//!    and the state of the parent node.
//!

#ifndef CGENEALOGICTREE_HH
#define CGENEALOGICTREE_HH

#include <list>
#include <cstdlib>
#include <cstdint>

template<class CBPRNG_t>
class CNodeValue;

/// \brief Class to hold and spawn nodes with associates pedigree information.
/// The nodes are kept in a list
/// \tparam CBPRNG_t a counter-based random number generator from the Random123 library
template<typename CBPRNG_t>
class CGenealogicTree
{
public:
  using RandomEngine = CBPRNG_t; ///< Random engine type.
  using NodeType = CNodeValue<CBPRNG_t>; ///< Node type.
  using InternalDataType = std::list<NodeType*>; ///< Node storage type.
  /// \param maxN the maximum number of nodes parameter
  ///        used in the additional node state calculation
  CGenealogicTree(uint maxN);

  ~CGenealogicTree(); ///< Destructor.

  /// \brief create a daughter of current node
  /// \param node The parent of the created node
  /// \return pointer to the created node
  NodeType* SpawnNode(NodeType* node);

  /// \brief create the first node in the tree
  /// \param e The additional state of the node
  /// \return pointer to the created node
  NodeType* EmplaceTopNode(int e = 0, uint64_t aHashValue = 0); // TODO add an rng state parameter

  /// \brief get the number of nodes
  /// \return the number of nodes stored in the tree
  size_t GetTotalNumberOfNodes() const;

  /// \brief Get the container storing nodes.
  /// \return a constant reference to the tree.
  InternalDataType const& GetInternalData() const {return data;}

  /// \brief Get the maximum number on nodes parameter.
  /// \return fMaxN
  uint GetMaxN() const {return fMaxN;}

  /// \brief Get the random engine.
  /// \return a constant pointer to the random engine used by all nodes.
  RandomEngine const* GetRandomEngine() const;

private:
  InternalDataType data; ///< The tree storing the nodes.
  uint fMaxN; ///< The maximum number of nodes parameter for the additional state calclulation.
  RandomEngine* fRandomEngine; ///< The random engine used by all nodes.
};

/// \brief checks if two CGenealogicTrees are equal
/// \return true if and only if the maximum number of nodes
///         and the trees of nodes are equal,
///         otherwise returns false.
///         the current state of the random engine is ignored.
/// \tparam CBPRNG_t a counter-based random number generator from the Random123 library
template<typename CBPRNG_t>
bool operator==(CGenealogicTree<CBPRNG_t> const& rhs, CGenealogicTree<CBPRNG_t> const& lhs);

#endif // CGENEALOGICTREE_HH
