//!    \file CNodeValue.hh
//!    \brief Class for the generator state with additional information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#ifndef CNODEVALUE_HH
#define CNODEVALUE_HH

#include <cstdint>
#include <cstdlib>

template<typename CBPRNG_t>
class CRNGState;

/// \brief Class to hold nodes with associates pedigree information
/// Able to construct daughter nodes with the proper derived state.
/// \tparam CBPRNG_t a counter-based random number generator from the Random123 library
template<typename CBPRNG_t>
class CNodeValue
{
public:
  using StateType = CRNGState<CBPRNG_t>;

  /// \brief Create node with fixed state.
  /// \param CrngState random number generator state
  /// \param hashValue hash value further to be used for seeding the generator
  /// \param e additional state
  CNodeValue(int e, size_t aHashValue,
             const StateType* crngState = nullptr);

  CNodeValue(CNodeValue const& value); ///< \brief Copy constructor.
  
  ~CNodeValue(); ///< \brief Destructor;

  /// \brief Create a new daughter given the sibling information
  /// \param nodeNumber the amount of nodes after this one is created.
  ///        Inherited from the generic implementation,
  ///        Now is unused because the information is present in \ref fCRNGState
  /// \param maxN the maximum number of children of a node
  CNodeValue SpawnNode(uint32_t maxN, CBPRNG_t* aRandomEngine);

  int GetE() const {return fE;} ///< Get the additional state information \ref fE

   /// Get the hash value \ref fHashValue
  uint64_t GetHashValue() const {return fHashedPedigree;}

  /// Get the number of children  \ref fNumberOfChildren
  uint32_t GetNumberOfChildren() const {return fNumberOfChildren;}

  /// Get the random number generator state \ref fCRNGState
  const StateType *GetRNGState() const;

private:
  int fE;                          ///< Additional state
  uint64_t fHashedPedigree;        ///< hash value used for seeding the generator
  StateType* fCRNGState; ///< Random number generator state
  uint32_t fNumberOfChildren;      ///< Number of the generated children
};

/// \brief checks if two nodes are equal
/// \return true if and only if the additional states
///         and the random number generator states are equal
/// \tparam CBPRNG_t a counter-based random number generator from the Random123 library
template<typename CBPRNG_t>
bool operator==(CNodeValue<CBPRNG_t> const& rhs, CNodeValue<CBPRNG_t> const& lhs);

#endif // CNODEVALUE_HH
