//!    \file ExtendedEngineFactory.hh
//!    \brief Declares an engine factory with support for additional engines.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#ifndef EXTENDEDENGINEFACTORY_HH
#define EXTENDEDENGINEFACTORY_HH

#include "CLHEP/Random/EngineFactory.h"

namespace CLHEP
{

/// \brief Extends EngineFactory from CLHEP to support new engines like \ref G4Cbprng. <br>
/// Inherits EngineFactory that creates HepRandomEngines from state.
/// Currently adds support for \ref G4Cbprng <r123::Philox2x32>
/// and \ref G4Cbprng <r123::Threefry2x32>
class ExtendedEngineFactory: public EngineFactory
{
public:

  /// \brief Constructs a new engine from the state obtaine from an input stream.
  /// \param is the stream to read the state from
  /// \return a pointer to the new random engine if the state is valid for a HepRandomEngine,
  ///         null pointer otherwise.
  static HepRandomEngine* newEngine(std::istream & is);

  /// \brief Constructs a new engine from the state obtaine from a vector.
  /// \param v a vector containing a state of an engine
  /// \return a pointer to the new random engine if the state is valid for a HepRandomEngine,
  ///         null pointer otherwise.
  static HepRandomEngine* newEngine(std::vector<unsigned long> const & v);
};

}
#endif // EXTENDEDENGINEFACTORY_HH
