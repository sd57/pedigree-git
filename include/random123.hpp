//!    \file random123.hpp
//!    Header that includes headers from the Random123 library.
//!
//!    \copyright Copyright (c) 2017 Dmitry Savin <sd57@protonmail.ch>
//!    \license Distributed under the GNU General Public License version 3.
//!
//!    -Wexpansion-to-defined clang diagnostic is suppressed
//!    because otherwise it is emitted multiple times per file
//!    and makes the output unreadable.
//!    An additional warning is emitted to not hide the possible
//!    dependence on the compiler.
//!    To avoid duplicating the boilerplate code,
//!    all headers needed for the project are included at once.

#ifndef RANDOM123_HPP
#define RANDOM123_HPP

#ifdef __clang__
#warning "Disabling -Wexpansion-to-defined for clean compile output"
// TODO check portability
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexpansion-to-defined"
#endif
#include "array.h"
#include "philox.h"
#include "threefry.h"
#include "ReinterpretCtr.hpp"
#include "uniform.hpp"
#ifdef __clang__
#pragma clang diagnostic pop
// FIXME looks like the diagnostic still remains suppressed
#endif

#endif // RANDOM123_HPP
