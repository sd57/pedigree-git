//!    \file NodeValue.hh
//!    \brief Declares class for the generator state with additional information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#ifndef NODEVALUE_HH
#define NODEVALUE_HH

#include <cstdint>
#include <cstdlib>

class RNGState;

namespace CLHEP
{
  class HepRandomEngine;
}

/// \brief Class to hold nodes with associates pedigree information.
/// Able to construct daughter nodes with the proper derived state.
class NodeValue
{
public:
  using StateType = RNGState;
  using EngineType = CLHEP::HepRandomEngine;
  /// \brief Create node with fixed state.
  /// \param rngState random number generator state
  /// \param hashValue hash value further to be used for seeding the generator
  /// \param e additional state
  NodeValue(int e, size_t aHashValue, const StateType &rngState);
  NodeValue(int e, size_t aHashValue, EngineType* engine);

   NodeValue(NodeValue const& value); ///< \brief Copy constructor.
  ~NodeValue(); ///< \brief Destructor;

  /// \brief Create a new daughter given the sibling information.
  /// \param maxN the maximum number of children of a node
  /// \param engine random number generator to use for the additional state generation
  NodeValue SpawnNode(uint maxN, EngineType* engine);

  int GetE() const {return fE;} ///< Get the additional state information \ref fE

  uint64_t GetHashValue() const {return fHashedPedigree;} ///< Get the hash value \ref fHashValue

  /// Get the number of children  \ref fNumberOfChildren
  uint32_t GetNumberOfChildren() const {return fNumberOfChildren;}

  /// Get the random number generator state \ref fRNGState.
  /// \return a pointer to the generator state
  const StateType* GetRNGState() const;

private:
  int fE; ///< Additional state
  uint64_t fHashedPedigree; ///< hash value used for seeding the generator
  StateType* fRNGState; ///< Random number generator state
  uint32_t fNumberOfChildren; ///< Number of the generated children
};

/// \brief Checks if two nodes are equal.
/// \return true if and only if the additional states, hash values
///         and the random number generator states are equal
bool operator==(NodeValue const& rhs, NodeValue const& lhs);

#endif // NODEVALUE_HH
