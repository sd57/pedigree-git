//!    \file RNGState.hh
//!    \brief Declares a class storing both the key and the counter for a Random123 engine.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#ifndef RNGSTATE_HH
#define RNGSTATE_HH

#include "CLHEP/Random/RandomEngine.h" // for the type deduction only

/// \brief Class to store a HepRandomEngine state.
/// Abstracts the concrete state implementation
/// and adds some debug output when needed.
class RNGState
{
  using RandomEngine = CLHEP::HepRandomEngine; ///< random engine type

  /// random engine state type
  using CLHEPStateType = decltype((static_cast<RandomEngine*>(nullptr))->put());

public:
  RNGState(); ///< Default constructor

  /// Constructor that gets the state of a random engine and stores it
  /// \param engine an initialized HepRandomEngine
  RNGState(const RandomEngine& engine);

  RNGState(RNGState&& state); ///< Move constructor

  RNGState(RNGState const& state); ///< Copy constructor

  RNGState const& operator=(RNGState const& state); ///< Copy assignment

  /// Constructor setting a fixed state.
  /// \param engineState generator state
  RNGState(CLHEPStateType const& engineState);

  ~RNGState(); ///< Destructor

  /// Get the stored engine state.
  /// \return the engine state \ref fEngineState.
  CLHEPStateType const& GetEngineState() const {return fEngineState;}

  /// Compare the engine states.
  /// \returns true if the states are equal, false otherwise.
  bool operator==(RNGState const& lhs) const;

private:
  /// \brief a \ref RandomEngine state.
  /// Must be default construcrable and deletable.
  CLHEPStateType fEngineState;
};

#endif // RNGSTATE_HH
