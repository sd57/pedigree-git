//!    \file CRNGState.hh
//!    \brief Declares a class storing both the key and the counter for a Random123 engine
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.
//!

#ifndef CRNGSTATE_HH
#define CRNGSTATE_HH

#include <type_traits>

#include "random123.hpp"

/// \brief Class to hold both the key and the counter for a Random123 engine
/// Restricted to engines that yield 64-bit random values
/// \tparam CBPRNG_t a counter-based random number generator from the Random123 library
template<typename CBPRNG_t>
class CRNGState
{
public:
  using ctr_type = typename CBPRNG_t::ctr_type; ///< Counter type
  using key_type = typename CBPRNG_t::key_type; ///< Key type
  /// The type of the value yielded by the enerator
  using ret_type = typename std::result_of<CBPRNG_t(ctr_type, key_type)>::type;
  /// Constructor with fixed key and counter
  /// \param aCtr counter, defaults to 0
  /// \param aKey key, defaults to an empty initializer list
  CRNGState(ctr_type aCtr = {{0}}, key_type aKey = {{}});
  CRNGState(CRNGState &&state); ///< Move constructor
  CRNGState(CRNGState const& state); ///< Copy constructor
  ~CRNGState(); ///< Destructor
  /// Equality operator.
  /// \return true if and only if both the keys and the counters are equal
  bool operator==(CRNGState const& lhs) const;
  /// \brief Inscreases the key.
  /// Starts with the last element of the key.
  /// If it overflows, continues withe previous element and so on.
  void IncreaseKey();
  /// \brief Inscreases the counter.
  /// Starts with the last element of the counter.
  /// If it overflows, continues withe previous element and so on.
  void IncreaseCtr();
  ctr_type GetCtr(); ///< Get a copy of the counter
  key_type GetKey(); ///< Get a copy of the key
  void SetCtr(ctr_type const& aCtr);///< Set the counter
private:
  ctr_type fCtr; ///< Counter
  key_type fKey; ///< Key
  /// Assert to make sure that the returned value is an array of one 64-bit integer
  static_assert(std::is_same<ret_type, r123::Array1x64>::value,
                "bad return type, must be an array of 1 64-bit unsigned integer");
};

extern template class CRNGState<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>>;
extern template class CRNGState<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>>;

#endif // CRNGSTATE_HH
