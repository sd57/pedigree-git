//!    \file GenealogicTree.hh
//!    \brief Declares a tree structure that hold nodes containing pedigree information.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.
//!
//!    The structure is a tree from tree.hh library by Kasper Peeters.
//!    It allows initializing the root node with a parameter to produce different trees
//!    and spawning daughter nodes.
//!    The state of the subsequent nodes is defined by the number of previously spawn siblings
//!    and the state of the parent node.

#ifndef GENEALOGICTREE_HH
#define GENEALOGICTREE_HH

#include <list>
#include <cstdlib> // for size_t and uint

class NodeValue;

namespace CLHEP
{
  class HepRandomEngine;
}

/// \brief Class to hold and spawn nodes with associates pedigree information.
/// The nodes are kept in a list
class GenealogicTree
{
public:
  using RandomEngine = CLHEP::HepRandomEngine; ///< Random engine type.
  using NodeType = NodeValue; ///< Node type.
  using InternalDataType = std::list<NodeType*>; ///< Node storage type.
  /// Constructor setting the used engine for all nodes and maximum number of nodes.
  GenealogicTree(RandomEngine const& engine, uint maxN);

  ~GenealogicTree(); ///< Destructor.

  /// \brief create a daughter of current node.
  /// \param node The parent of the created node
  /// \return pointer to the created node
  NodeType* SpawnNode(NodeType* node);

  /// \brief Create the first node in the tree.
  /// \param e The additional state of the node
  /// \param hashValue hash value to be used for the generator seeding
  /// \return pointer to the created node
  NodeType* EmplaceTopNode(int e = 0, size_t hashValue = 0);

  /// \brief Get the number of nodes.
  /// \return the number of nodes stored in the tree
  size_t GetTotalNumberOfNodes() const;

  /// \brief Get the container storing nodes.
  /// \return a constant reference to the tree.
  InternalDataType const& GetInternalData() const {return data;}

  /// \brief Get the maximum number on nodes parameter.
  /// \return fMaxN
  uint GetMaxN() const {return fMaxN;}

  /// \brief Get the random engine.
  /// \return a constant pointer to the random engine used by all nodes.
  RandomEngine const* GetRandomEngine() const;

private:
  InternalDataType data; ///< The tree storing the nodes.
  uint fMaxN; ///< The maximum number of nodes parameter for the additional state calclulation.
  RandomEngine* fRandomEngine; ///< The random engine used by all nodes.
};

/// \brief Checks if two GenealogicTrees are equal.
/// \return true if and only if the maximum number of nodes
///         and the trees of nodes are equal,
///         otherwise returns false.
///         the current state of the random engine is ignored.
bool operator==(GenealogicTree const& rhs, GenealogicTree const& lhs);

#endif // GENEALOGICTREE_HH
