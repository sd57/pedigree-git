//!    \file test2.cc
///    \brief Tests to check the independence of the result on node spawn order.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "CGenealogicTree.hh"
#include "CNodeValue.hh"
#include "CRNGState.hh"
#include "random123.hpp"
#include "array.h"

#include <functional>
#include <vector>
#include <iostream>
#include <chrono>

#ifdef NDEBUG
static_assert(false, "DNDEBUG mut be unset for the test to fail properly");
#endif

/// compile-time unsigned integer powers
constexpr uint uintpowuint(uint b, uint p, uint result = 1) {
  return p ? uintpowuint(b*b, p/2, (p % 2) ? result*b : result) : result;
}

namespace
{
constexpr bool timingOutput = false;
constexpr bool debugOutput = false;
template<typename TEngine> void TestIndependenceFromOrder(TEngine& anEngine);
template<typename TEngine> void RunTestsForEngine();
} // anonymous namespace

int main()
{
  RunTestsForEngine<r123::ReinterpretCtr<r123array1x64, r123::Philox2x32>>();
  RunTestsForEngine<r123::ReinterpretCtr<r123array1x64, r123::Threefry2x32>>();
  return 0;
}

namespace
{

template<typename TEngine> void RunTestsForEngine()
{
  TEngine anEngine;
  
  auto const start = std::chrono::steady_clock::now();

  TestIndependenceFromOrder(anEngine);

  if (timingOutput)
  {
    auto const stop = std::chrono::steady_clock::now();
    auto const diff = stop - start;
    std::cout << "test finished in "
              << std::chrono::duration<double, std::milli>(diff).count()
        << std::endl;
  }
}

template<typename TEngine>
void TestIndependenceFromOrder(TEngine& anEngine)
{
  using CGenealogicTree_t = CGenealogicTree<TEngine>;
  auto PutTopNode = [](CGenealogicTree<TEngine>& Cgenealogictree)
  {
    return Cgenealogictree.EmplaceTopNode(291702489);
  };

  CGenealogicTree_t theCGenealogicTree(3837947779);
  auto topNode = PutTopNode(theCGenealogicTree);

  // TODO get the lambda return type without invoking
  using NodepointerType = decltype(topNode);
  // spawns nNodesPerLayer nodes and stores them in a vector
  // then spawns nNodesPerLayer for each of them until nLayers reached
  auto WidthSpawn = [](CGenealogicTree_t& Cgenealogictree, NodepointerType topNode,
      size_t nNodesPerLayer, size_t nLayers)
  {
    std::vector<NodepointerType> currentNodes(1, topNode);
    std::vector<NodepointerType> newNodes;
    for(size_t layerN = 0; layerN < nLayers; ++layerN)
    {
      for(auto it : currentNodes)
      {
        for(size_t childN = 0; childN < nNodesPerLayer; ++childN)
        {
          newNodes.push_back(Cgenealogictree.SpawnNode(it));
        }
      }
      currentNodes = newNodes;
      newNodes.clear();
    }
  };

  // spawns recursively nNodesPerLayer starting with the freshly inserted node
  // until depth nLayers is reached
  std::function<void(CGenealogicTree_t&, NodepointerType, size_t, size_t)> DepthSpawn =
      [&DepthSpawn](CGenealogicTree_t& Cgenealogictree, NodepointerType topNode,
      size_t nNodesPerLayer, size_t nLayers)
  {
    if(nLayers) for(size_t childN = 0; childN < nNodesPerLayer; ++childN)
    {
      auto newNode = Cgenealogictree.SpawnNode(topNode);
      DepthSpawn(Cgenealogictree, newNode, nNodesPerLayer, nLayers - 1);
    }
  };

  static size_t const nodesPerLayer = 4;
  static size_t const layers = 6;

  WidthSpawn(theCGenealogicTree, topNode, nodesPerLayer, layers);

  CGenealogicTree_t theCGenealogicTreeCopy(3837947779);
  auto topCopyIterator = PutTopNode(theCGenealogicTreeCopy);
  DepthSpawn(theCGenealogicTreeCopy, topCopyIterator, nodesPerLayer, layers);

  // test the number of nodes in each tree
  auto const nodesExpected = (uintpowuint(nodesPerLayer, layers+1) - 1)/(nodesPerLayer - 1);
  if(debugOutput)
    std::cout << "testing the total number of nodes, expected " << nodesExpected
              << ", in the width-first " << theCGenealogicTree.GetTotalNumberOfNodes()
              << ", in the depth-first " << theCGenealogicTree.GetTotalNumberOfNodes()
              << std::endl;
  assert((theCGenealogicTree.GetTotalNumberOfNodes() == nodesExpected));
  assert((theCGenealogicTreeCopy.GetTotalNumberOfNodes() == nodesExpected));

  // test that the Cgenealogictrees with different spawn order are equal
  assert((theCGenealogicTree == theCGenealogicTreeCopy));
}
} // anonymous namespace


