//!    \file test1.cc
///    \brief Tests to check the independence of the result on node spawn order.
//!
//!    \authors Author:  Dmitry Savin <sd57@protonmail.ch> <br>
//!             Development sponsored by Google in Google Summer of Code 2017 <br>
//!             Supervision / code review: John Apostolakis & Sandro Wenzel.
//!
//!    \license Distributed under the GNU Lesser General Public License version 3.

#include "GenealogicTree.hh"
#include "NodeValue.hh"
#include "RNGState.hh"

#include "CLHEP/Random/DualRand.h"
#include "CLHEP/Random/JamesRandom.h"
#include "CLHEP/Random/MixMaxRng.h"
#include "CLHEP/Random/MTwistEngine.h"
#include "CLHEP/Random/RanecuEngine.h"
#include "CLHEP/Random/Ranlux64Engine.h"
#include "CLHEP/Random/RanluxEngine.h"
#include "CLHEP/Random/RanshiEngine.h"

#include "G4PCbprng.hh"
#include "random123.hpp"

#include <functional>
#include <iostream>
#include <chrono>
#include <type_traits>

#ifdef NDEBUG
static_assert(false, "DNDEBUG mut be unset for the test to fail properly");
#endif

/// compile-time unsigned integer powers
constexpr uint uintpowuint(uint b, uint p, uint result = 1) {
  return p ? uintpowuint(b*b, p/2, (p % 2) ? result*b : result) : result;
}

namespace
{
constexpr bool timingOutput = false;
constexpr bool debugOutput = false;
void TestSaveLoad(CLHEP::HepRandomEngine& anEngine);
void TestIndependenceFromOrder(CLHEP::HepRandomEngine& anEngine);
template<typename TEngine> void RunTestsForEngine();
} // anonymous namespace

int main()
{
  RunTestsForEngine<CLHEP::RanecuEngine>();
  RunTestsForEngine<CLHEP::HepJamesRandom>();
  RunTestsForEngine<CLHEP::MixMaxRng>();
  RunTestsForEngine<CLHEP::MTwistEngine>();
  RunTestsForEngine<CLHEP::Ranlux64Engine>();
  RunTestsForEngine<CLHEP::RanluxEngine>();
  RunTestsForEngine<CLHEP::RanshiEngine>();
  RunTestsForEngine<CLHEP::G4PCbprng<r123::Philox2x32>>();
  RunTestsForEngine<CLHEP::G4PCbprng<r123::Threefry2x32>>();
  return 0;
}

namespace
{

template<typename TEngine> void RunTestsForEngine()
{
  static_assert(std::is_base_of<CLHEP::HepRandomEngine, TEngine>::value,
                "TEngine must be derived from CLHEP::HepRandomEngine");
  TEngine anEngine;
  auto const start = std::chrono::steady_clock::now();
  TestSaveLoad(anEngine);
  TestIndependenceFromOrder(anEngine);
  if(timingOutput)
  {
    auto const stop = std::chrono::steady_clock::now();
    auto const diff = stop - start;
    std::cout << "test for " << anEngine.name()
              << " finished in " << std::chrono::duration<double, std::milli> (diff).count()
        << std::endl;
  }
}

void TestSaveLoad(CLHEP::HepRandomEngine& anEngine)
{
  anEngine.saveStatus();
  if(debugOutput) anEngine.showStatus();
  double const value1 = anEngine.flat();
  if(debugOutput) anEngine.showStatus();
  anEngine.restoreStatus();
  if(debugOutput) anEngine.showStatus();
  double const value2 = anEngine.flat();
  if(debugOutput)
  {
    std::cout << "value1 = " << value1
              << ", value2 = " << value2
              << std::endl;
    anEngine.showStatus();
  }
  assert((value1 == value2));
}

void TestIndependenceFromOrder(CLHEP::HepRandomEngine& anEngine)
{
  anEngine.saveStatus();
  auto PutTopNode = [](GenealogicTree& pedigree)
  {
    return pedigree.EmplaceTopNode(291702489, 1689758207);
  };

  if(debugOutput)
    std::cout << "Using engine " << anEngine.name()
              << std::endl;

  GenealogicTree theGenealogicTree(anEngine, 3837947779);
  auto topmostNode = PutTopNode(theGenealogicTree);

  // spawns nNodesPerLayer nodes and stores them in a vector
  // then spawns nNodesPerLayer for each of them until nLayers reached
  auto WidthSpawn = [](GenealogicTree& pedigree, NodeValue* topNode,
      size_t nNodesPerLayer, size_t nLayers)
  {
    std::vector<NodeValue*> currentNodes(1, topNode);
    std::vector<NodeValue*> newNodes;
    for(size_t layerN = 0; layerN < nLayers; ++layerN)
    {
      for(auto it : currentNodes)
      {
        for(size_t childN = 0; childN < nNodesPerLayer; ++childN)
        {
          newNodes.push_back(pedigree.SpawnNode(it));
        }
      }
      currentNodes = newNodes;
      newNodes.clear();
    }
  };

  // spawns recursively nNodesPerLayer starting with the freshly inserted node
  // until depth nLayers is reached
  std::function<void(GenealogicTree&, NodeValue*, size_t, size_t)> DepthSpawn =
      [&DepthSpawn](GenealogicTree& pedigree, NodeValue* topNode,
      size_t nNodesPerLayer, size_t nLayers)
  {
    if(nLayers) for(size_t childN = 0; childN < nNodesPerLayer; ++childN)
    {
      auto newNode = pedigree.SpawnNode(topNode);
      DepthSpawn(pedigree, newNode, nNodesPerLayer, nLayers - 1);
    }
  };

  static size_t const nodesPerLayer = 3;
  static size_t const layers = 4;

  WidthSpawn(theGenealogicTree, topmostNode, nodesPerLayer, layers);

  anEngine.restoreStatus();
  GenealogicTree theGenealogicTreeCopy(anEngine, 3837947779);
  auto topCopyNode = PutTopNode(theGenealogicTreeCopy);
  DepthSpawn(theGenealogicTreeCopy, topCopyNode, nodesPerLayer, layers);

  // test the number of nodes in each tree
  auto const nodesExpected = (uintpowuint(nodesPerLayer, layers+1) - 1)/(nodesPerLayer - 1);
  if(debugOutput)
    std::cout << "testing the total number of nodes, expected " << nodesExpected
              << ", in the width-first " << theGenealogicTree.GetTotalNumberOfNodes()
              << ", in the depth-first " << theGenealogicTree.GetTotalNumberOfNodes()
              << std::endl;
  assert((theGenealogicTree.GetTotalNumberOfNodes() == nodesExpected));
  assert((theGenealogicTreeCopy.GetTotalNumberOfNodes() == nodesExpected));

  // test that the pedigrees with different spawn order are equal
  assert((theGenealogicTree == theGenealogicTreeCopy));
}
} // anonymous namespace


