cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

MACRO(BUILDTEST testName)
  INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/include)
  INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/tree/include)

  file(GLOB sources ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cc)
  file(GLOB headers ${CMAKE_CURRENT_SOURCE_DIR}/include/*.hh)

  add_executable(${testName} ${testName}.cc ${sources} ${headers})
  set_property(TARGET ${testName} PROPERTY CXX_STANDARD 14)
  set_property(TARGET ${testName} PROPERTY CXX_STANDARD_REQUIRED)
  target_link_libraries (${testName} pedigree)
ENDMACRO()
